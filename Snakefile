
configfile: "config.yml"


rule all:
    input:
        ["{path}/results/trimming/{sample}_R{pair}.fastq.gz".format(
           path = config["path"], sample = s, pair = p) for s in config['samples'] for p in [1,2]], 

        ["{path}/results/align/index/{file}".format(
           path = config["path"], file = f) for f in ['Genome', 'SA', 'SAindex']], 

        ["{path}/results/align/{sample}/{sample}Aligned.sortedByCoord.out.bam".format(
           path = config["path"], sample = s) for s in config['samples']], 

        ["{path}/results/dedup/{sample}.bam".format(
           path = config["path"], sample = s) for s in config['samples']], 

        ["{path}/results/splitncigarreads/{sample}.bam".format(
           path = config["path"], sample = s) for s in config['samples']], 

        ["{path}/results/fixed-rg/{sample}.bam".format(
           path = config["path"], sample = s) for s in config['samples']], 

        ["{path}/results/fixed-rg/{sample}.bam.bai".format(
           path = config["path"], sample = s) for s in config['samples']], 
        
        ["{path}/results/variant/{sample}.vcf".format(
           path = config["path"], sample = s) for s in config['samples']], 


rule trimming_fastp:
    input:
        r1 = expand("{path_raw}/merged/{{sample}}_R1.fastq.gz", path_raw = config['rawdata']),
        r2 = expand("{path_raw}/merged/{{sample}}_R2.fastq.gz", path_raw = config['rawdata']),
    output:
        r1 = "{path}/results/trimming/{sample}_R1.fastq.gz",
        r2 = "{path}/results/trimming/{sample}_R2.fastq.gz",
        r1_unp = "{path}/results/trimming/unpaired/{sample}_R1.fastq.gz",
        r2_unp = "{path}/results/trimming/unpaired/{sample}_R2.fastq.gz",
    log:
        json = "{path}/reports/trimming/{sample}.json", 
        html = "{path}/reports/trimming/{sample}.html",
        out  = "{path}/logs/trimming/{sample}.out",
        err  = "{path}/logs/trimming/{sample}.err",
    threads: 
        config['fastp']['threads']
    container: 
        config['fastp']['container']
    resources:
        time=lambda wildcards, attempt: attempt * config['fastp']['time'],
        mem=lambda wildcards, attempt: attempt * 2**10 * config['fastp']['mem']
    shell:
        """
        fastp   -i {input.r1} \
                -I {input.r2} \
                -o {output.r1} \
                -O {output.r2} \
                --unpaired1 {output.r1_unp} \
                --unpaired2 {output.r2_unp} \
                -h {log.html} \
                -j {log.json} \
                -w {threads} \
                > {log.out} \
                2> {log.err}
        """
        

rule star_aligner_index:
    input:
        ref = config['star']['index']['reference']
    output:
        idx = expand("{{path}}/results/align/index/{files}", files = ['Genome', 'SA', 'SAindex'])
    params:
        idx = "{path}/results/align/index",
        prefix = "{path}/results/align/index/reference",
        mem = config['star']['index']['memory'],
    threads: 
        config['star']['index']['threads']
    log:
        out = "{path}/logs/align/index.out",
        err = "{path}/logs/align/index.err",
    container: 
        config['star']['container']
    resources:
        time=lambda wildcards, attempt: attempt * config['star']['index']['time'],
        mem=lambda wildcards, attempt: attempt * 2**10 * config['star']['index']['mem']
    shell:
        """
        STAR    --runThreadN {threads} \
                --runMode genomeGenerate \
                --genomeDir {params.idx} \
                --genomeFastaFiles {input.ref} \
                --outFileNamePrefix {params.prefix} \
                --limitGenomeGenerateRAM {params.mem} \
                > {log.out} \
                2> {log.err}
        """
        
        
rule star_aligner_map:
    input:
        r1 = "{path}/raw_data/merged/{sample}_R1.fastq.gz",
        r2 = "{path}/raw_data/merged/{sample}_R2.fastq.gz",
        idx = expand("{{path}}/results/align/index/{files}", files = ['Genome', 'SA', 'SAindex'])
    output:
        aln = "{path}/results/align/{sample}/{sample}Aligned.sortedByCoord.out.bam"
    params:
        idx = "{path}/results/align/index",
        prefix = "{path}/results/align/{sample}/{sample}",
        dir = "{path}/results/align/{sample}"
    threads: 
        config['star']['align']['threads']
    log:
        out = "{path}/logs/align/index/{sample}.out",
        err = "{path}/logs/align/index/{sample}.err",
    container: 
        config['star']['container']
    resources:
        time=lambda wildcards, attempt: attempt * config['star']['align']['time'],
        mem=lambda wildcards, attempt: attempt * 2**10 * config['star']['align']['mem']
    shell:
        """
        STAR    --runThreadN {threads} \
                --runMode alignReads \
                --genomeDir {params.idx} \
                --readFilesIn {input.r1} {input.r2} \
                --outFileNamePrefix {params.prefix} \
                --outSAMtype BAM SortedByCoordinate \
                --outSAMunmapped Within \
                --readFilesCommand zcat \
                > {log.out} \
                2> {log.err}
        """


rule mark_duplicates:
    input:
        aln = "{path}/results/align/{sample}/{sample}Aligned.sortedByCoord.out.bam"
    output:
        bam = "{path}/results/dedup/{sample}.bam",
        metrics="{path}/reports/dedup/{sample}.metrics.txt"
    log:
        out = "{path}/logs/picard/dedup/{sample}.log",
        err = "{path}/logs/picard/dedup/{sample}.err"
    params:
        gatk_params="ASSUME_SORTED=true",
        java_opts=""#"-Xmx12G" # java memory alloc 12Gb
    container:
        config['picard']['container']
    resources:
        time=lambda wildcards, attempt: attempt * config['picard']['time'],
        mem=lambda wildcards, attempt: attempt * 2**10 * config['picard']['mem']
    shell:
        """
        picard  MarkDuplicates \
                {params.java_opts} \
                {params.gatk_params} \
                I={input} \
                OUTPUT={output.bam} \
                METRICS_FILE={output.metrics} \
                > {log.out} \ 
                2> {log.err}
        """

rule splitncigarreads:
    input:
        bam = "{path}/results/dedup/{sample}.bam",
        ref = config['reference']
    output:
        bam = "{path}/results/splitncigarreads/{sample}.bam"
    log:
        out = "{path}/logs/gatk/splitNCIGARreads/{sample}.log",
        err = "{path}/logs/gatk/splitNCIGARreads/{sample}.err"
    params:
        extra="",  # optional
        java_opts="",  # optional
    container:
        config['gatk']['container']
    resources:
        time=lambda wildcards, attempt: attempt * config['gatk']['time'],
        mem=lambda wildcards, attempt: attempt * 2**10 * config['gatk']['mem']
    shell:
        """
        gatk    --java-options '{params.java_opts}' SplitNCigarReads \
                {params.extra} \
                -R {input.ref} \
                -I {input.bam} \
                -O {output.bam} \
                > {log.out} \
                2> {log.err}
        """

rule replace_rg:
    input:
        bam = "{path}/results/splitncigarreads/{sample}.bam"
    output:
        bam = "{path}/results/fixed-rg/{sample}.bam"
    log:
        out = "{path}/logs/picard/replace_rg/{sample}.log",
        err = "{path}/logs/picard/replace_rg/{sample}.err"
    params:
        extra = "RGLB=KP_RNASeq_variants RGPL=illumina RGPU={sample} RGSM={sample}",
        java_opts = ""
    container:
        config['picard']['container']
    resources:
        time=lambda wildcards, attempt: attempt * config['picard']['time'],
        mem=lambda wildcards, attempt: attempt * 2**10 * config['picard']['mem']
    shell:
        """
        picard  AddOrReplaceReadGroups \
                {params.java_opts} \
                {params.extra} \
                I={input.bam} \
                O={output.bam} \
                > {log.out} \ 
                2> {log.err}
        """

rule samtools_index:
    input:
        bam = "{path}/results/fixed-rg/{sample}.bam"
    output:
        bai = "{path}/results/fixed-rg/{sample}.bam.bai"
    log:
        "{path}/logs/samtools_index/{sample}.log"
    params:
        "" # optional params string
    threads: 
        config['samtools']['threads']
    container:
        config['samtools']['container']
    resources:
        time=lambda wildcards, attempt: attempt * config['samtools']['time'],
        mem=lambda wildcards, attempt: attempt * 2**10 * config['samtools']['mem']
    shell:
        """
        samtools    index \
                    -@ {threads} \
                    {params} \
                    {input.bam} \
                    {output.bai} \
                    {log}
        """

rule mutect2:
    input:
        ref = config['reference'],
        bam = "{path}/results/fixed-rg/{sample}.bam",
        bai = "{path}/results/fixed-rg/{sample}.bam.bai"
    output:
        vcf = "{path}/results/variant/{sample}.vcf"
    message:
        "Testing Mutect2 with {wildcards.sample}"
    threads:
        #   config['gatk']['mutect2']['threads']
        workflow.cores
    params:
        extra = "",
        java_opts = f"-Xmx{config['gatk']['mutect2']['mem']}G"
    container:
        config['gatk']['container']
    resources:
        time=lambda wildcards, attempt: attempt * config['gatk']['mutect2']['time'],
        mem=lambda wildcards, attempt: attempt * 2**10 * config['gatk']['mutect2']['mem']
    log:
        out = "{path}/logs/mutect/mutect_{sample}.log",
        err = "{path}/logs/mutect/mutect_{sample}.err"
    shell:
        """
        gatk    --java-options '{params.java_opts}' Mutect2 \
                --input {input.bam} \
                --output {output.vcf} \
                --reference {input.ref} \
                {params.extra} \
                > {log.out} \
                2> {log.err}
        """
